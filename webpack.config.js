/**
 * Created by Alexandre on 29/03/2017.
 */
const webpack = require('webpack')
var path = require('path');

module.exports = {
    entry: './src/main.js',
    output: {
        path: path.join(__dirname, ''),
        publicPath: '/',
        filename: 'bundle.js',
    }
}