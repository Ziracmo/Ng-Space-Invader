# New Generation Space Invader !

## Prerequisites

To be able to run this project you have to get NodeJS en npm on your global packages first

> On Windows

### NodeJS and npm

Node is use to make a server side with Javascript and npm to load distant repository mainly

Go to [NodeJS](https://nodejs.org/en/) and download the last recommended version of NodeJS
When you install it, make sure you have npm install in same time

> On Linux/ Mac

Google is your friend :D

## Installation

### Webpack

If NodeJS and npm are installed you should have no worries about this part.
Webpack is used to compile all the js files in an unique one, and to serve them to a local port in option.

```
npm install -g webpack
```

*It may need to do this command as an administrator :3*

### Local Project dependencies

In the command line, in you project location use this command :

```
npm install
```

It should install all the dependencies set in the *package.json* locates in the project root.
Then a wonderful folder with the name of *node_modules* should appears in the the project root, don't be affraid.

## Run the project

To run the project you'll have two specials console commands :

1. It should build the project files locate in *./src*

 ```
 npm start
 ```

1. It should also build the project files in *./src* and serve them here : http://localhost:8070
```
npm run dev
 ```

![Alt text](http://octodex.github.com/images/stormtroopocat.jpg "The Stormtroopocat")