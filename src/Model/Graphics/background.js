/**
 * Created by Alexandre on 29/03/2017.
 */

const {app, PIXI, backgrounLayer} = require('../../Component/core')

const Background = () => {

    const backgroundUrl = 'assets/Backgrounds/blue.png';

    const background = {
        create: () => {
            let baseTexture = new PIXI.Sprite.fromImage(backgroundUrl).texture.baseTexture;
            for(let i = 0; i < app.renderer.width; i += baseTexture.realWidth) {
                for(let j = 0; j < app.renderer.height; j += baseTexture.realHeight) {
                    let sprite =  new PIXI.Sprite.fromImage(backgroundUrl);
                    sprite.displayGroup = backgrounLayer;
                    sprite.x = i;
                    sprite.y = j;
                    app.stage.addChild(sprite)
                }
            }
        }
    }
    return background
}

module.exports = Background