/**
 * Created by Alexandre on 31/03/2017.
 */

const {PIXI, entitiesLayer, app} = require('../../Component/core')
const {gameEntities} = require('../../Component/entitesArray')

const Explosion = (x, y) => {

    let baseAssetsUrl = 'assets/Damage/playerShip1_damage';
    let explosionImages = [
        baseAssetsUrl + '3.png',
        baseAssetsUrl + '2.png',
        baseAssetsUrl + '1.png'
    ];

    let frames = [];

    for (let i = 0; i < 3; i++) {
        frames.push(PIXI.Texture.fromImage(explosionImages[i]));
    }
    ;

    let anim = new PIXI.extras.AnimatedSprite(frames);

    anim.x = x;
    anim.y = y;
    anim.duration = 300;
    anim.anchor.set(0.5);
    anim.scale.set(0.6);
    anim.tint = 0xB4B4B4
    anim.rotation = 0.5
    anim.animationSpeed = 0.15;
    anim.displayGroup = entitiesLayer

    const explosion = {
        id: 0,
        create: () => {
            anim.play();
            anim.begin = new Date().getTime();
            app.stage.addChild(anim)
            explosion.id = gameEntities.push(explosion)
        },
        update: () => {
            let time = anim.begin + anim.duration;
            if(time < new Date().getTime())
                explosion.remove()
        },
        remove: () => {
            app.stage.removeChild(anim);
            gameEntities.splice(explosion.id - 1 , 1)
            for(let i = explosion.id - 1 ; i < gameEntities.length; i++) {
                gameEntities[i].id--;
            }
        }
    }
    return explosion
}

module.exports = Explosion

