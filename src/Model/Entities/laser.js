/**
 * Created by Alexandre on 29/03/2017.
 */
const {app, PIXI, effetsLayer} = require('../../Component/core')
let {gameEntities} = require('../../Component/entitesArray')

const lasers = {
    blue: 'assets/Lasers/laserBlue01.png',
    red: 'assets/Lasers/laserRed01.png'
}

const Laser = (x, y, speed, color = 'blue', entity = 'player') => {
    const laser = {
        id: 0,
        type: 'laser',
        fromEntity: entity,
        sprite: new PIXI.Sprite.fromImage(lasers[color]),
        create: () => {
            if(entity == 'enemy')
                laser.sprite.rotation = Math.PI
            laser.sprite.position.set(x, y);
            laser.sprite.anchor.set(0.5);
            laser.sprite.displayGroup = effetsLayer;
            laser.id = gameEntities.push(laser);
            app.stage.addChild(laser.sprite)
        },
        update: () => {
            laser.sprite.y += speed;
            if (laser.sprite.y < -laser.sprite.height)
                laser.mustBeDestroyed = true
        },
        remove: () => {
            app.stage.removeChild(laser.sprite);
            gameEntities.splice(laser.id - 1, 1)
            for (let i = laser.id - 1; i < gameEntities.length; i++) {
                gameEntities[i].id--;
            }
        }
    }
    return laser;
}

module.exports = {Laser}