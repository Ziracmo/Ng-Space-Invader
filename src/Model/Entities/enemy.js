/**
 * Created by Alexandre on 29/03/2017.
 */

const {PIXI, app, entitiesLayer} = require('../../Component/core')
let {gameEntities} = require('../../Component/entitesArray')
const enemiesSkinsUrl = 'assets/Enemies/enemy';
const explosion = require('../Fx/explosion')
const {Laser} = require('./laser')

const Enemy = (x, y, color) => {

    let laserSound = new Howl({
        src: ['assets/Sounds/sfx_laser2.ogg'],
        volume: 0.3
    });

    let screamSound = new Howl({
        src: ['assets/Sounds/sfx_scream.ogg'],
        volume: 0.3,
        sprite: {scream: [800, 3000]}
    })

    const enemy = {
        id: 0,
        sprite: new PIXI.Sprite.fromImage(enemiesSkinsUrl + color.charAt(0).toUpperCase() + color.slice(1).toLowerCase() + '1.png'),
        type: 'enemy',
        speed: 3,
        create: () => {
            enemy.sprite.anchor.set(0.5);
            enemy.sprite.position.set(x, y)
            enemy.sprite.scale.set(0.4);
            enemy.sprite.displayGroup = entitiesLayer;
            enemy.id = gameEntities.push(enemy)
            app.stage.addChild(enemy.sprite)
        },
        shoot: () => {
            let laser = Laser(enemy.sprite.x, enemy.sprite.y + enemy.sprite.height / 4, 4, 'red', 'enemy')
            laser.create()
            laserSound.play()
        },
        update: () => {
            if(enemy.sprite.x > 780|| enemy.sprite.x < 20) {
                enemy.speed = -enemy.speed
                enemy.sprite.y += enemy.sprite.height + 10
            }
            enemy.sprite.x += enemy.speed
            let rand = Math.floor(Math.random() * 150);
            if(rand == 1)
                enemy.shoot()
        },
        remove: () => {
            screamSound.play('scream')
            let anim = explosion(enemy.sprite.x, enemy.sprite.y);
            anim.create();
            app.stage.removeChild(enemy.sprite);
            gameEntities.splice(enemy.id - 1 , 1)
            for(let i = enemy.id - 1 ; i < gameEntities.length; i++) {
                gameEntities[i].id--;
            }
        }
    }
    return enemy
}

module.exports = Enemy