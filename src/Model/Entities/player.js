/**
 * Created by Alexandre on 29/03/2017.
 */
const {app, PIXI, entitiesLayer} = require('../../Component/core');
const {Laser} = require('./laser');
let {gameEntities} = require('../../Component/entitesArray')

const Player = () => {
    let sound = new Howl({
        src: ['assets/Sounds/sfx_laser1.ogg'],
        volume: 0.3
    });

    const player = {
        life: 3,
        speed: 5,
        sprite: new PIXI.Sprite.fromImage('assets/playerShip3_blue.png'),
        type: 'player',
        isMovingLeft: false,
        isMovingRight: false,
        isAlive: true,
        isTouched: false,
        create: () => {
            player.sprite.anchor.set(0.5);
            player.sprite.position.set(app.renderer.width / 2, app.renderer.height - 50)
            player.sprite.scale.set(0.7);
            player.sprite.displayGroup = entitiesLayer;
            gameEntities.push(player);
            app.stage.addChild(player.sprite);
        },
        moveLeft: () => {
            player.isMovingLeft = true;
        },
        moveRight: () => {
            player.isMovingRight = true;
        },
        update: () => {
            if (!player.isMovingLeft || !player.isMovingRight) {
                if (player.isMovingRight && player.sprite.x < app.renderer.width - 50)
                    player.sprite.x += player.speed
                else if (player.isMovingLeft && player.sprite.x > 50)
                    player.sprite.x -= player.speed
            }
        },
        shoot: () => {
            if (player.isAlive) {
                let laser = Laser(player.sprite.x, player.sprite.y - player.sprite.height / 4, -5)
                laser.create()
                sound.play()
            }
        },
        remove: () => {
            player.life--;
            if (player.life <= 0)
                player.isAlive = false
        }
    }
    return player
}

module.exports = Player