const Background = require('./Model/Graphics/background')
const Player = require('./Model/Entities/player')
const Enemy = require('./Model/Entities/enemy')
const Keyboard = require('./Model/Input/keyboard')
const gameloop = require('./Component/gameloop')
const {app} = require('./Component/core')
require('howler');

//////// Background ////////
const background = Background();
background.create();

/////// Music ///////
let sound = new Howl({
    src: ['assets/Sounds/game_sound.mp3'],
    volume: 1,
    loop: true
})
// sound.play()
let isPLaying = false;

/////// Player ///////
const player = Player();
player.create();

////// Enemies ///////
for(let i = 100; i < app.renderer.width - 100 ; i += 58) {
    const enemy = Enemy(i, 100, 'black');
    enemy.create()
}

/////// Inputs //////
let left = Keyboard(81), right = Keyboard(68), space = Keyboard(32), mute = Keyboard(77);
left.press = player.moveLeft
left.release = () => {player.isMovingLeft = false}
right.press = player.moveRight
right.release = () => {player.isMovingRight = false}
space.press = player.shoot
mute.press = () => {
    if (isPLaying) {
        sound.stop();
        isPLaying = !isPLaying
    } else {
        sound.play();
        isPLaying = !isPLaying
    }
}

////// Gameloop /////
app.ticker.add((delta) => {
    if(player.isAlive) {
        gameloop()
    }
})




