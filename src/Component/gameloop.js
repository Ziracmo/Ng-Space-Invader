/**
 * Created by Alexandre on 30/03/2017.
 */
const {gameEntities} = require('./entitesArray');

const gameloop = () => {
    for (let entity of gameEntities) {
        entity.update();

        //// Laser Collision
        if (entity.type == 'laser') {
            let laserSprite = entity.sprite;
            for (let entityTwo of gameEntities) {
                let entitySprite = entityTwo.sprite
                if(entityTwo.type) {
                    if (entityTwo.type == 'enemy' || entityTwo.type == 'player') {
                        if (entity.fromEntity != entityTwo.type &&
                            laserSprite.x - laserSprite.width / 2 > entitySprite.x - entitySprite.width / 2 &&
                            laserSprite.x + laserSprite.width / 2 < entitySprite.x + entitySprite.width / 2 &&
                            laserSprite.y - laserSprite.height / 2 < entitySprite.y + entitySprite.height / 2 &&
                            laserSprite.y + laserSprite.height / 2 > entitySprite.y - entitySprite.height / 2) {
                            entity.remove();
                            entityTwo.remove();
                        }
                    }
                }

            }
        }
    }
}
module.exports = gameloop

