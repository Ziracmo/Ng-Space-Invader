/**
 * Created by Alexandre on 29/03/2017.
 */
const PIXI = require('pixi.js');
const disp = require('pixi-display');

const app = new PIXI.Application(800, 600, {backgroundColor: 0x2c3e50});
document.body.appendChild(app.view);

///// Game Layer /////
app.stage.displayList = new PIXI.DisplayList();

let backgrounLayer = new PIXI.DisplayGroup(0, false);
let effetsLayer = new PIXI.DisplayGroup(1, false);
let entitiesLayer = new PIXI.DisplayGroup(2, false);
let UILayer = new PIXI.DisplayGroup(3, false);

let score = 0;

module.exports = {
    PIXI,
    app,
    backgrounLayer,
    effetsLayer,
    entitiesLayer,
    UILayer,
    score
}